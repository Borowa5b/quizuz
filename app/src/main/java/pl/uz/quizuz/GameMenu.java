package pl.uz.quizuz;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import pl.uz.quizuz.api.FirebaseAccessor;

/**
 * Game main menu class
 *
 * @author Mateusz Borowski
 */
public class GameMenu extends AppCompatActivity {

    private CompositeDisposable compositeDisposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_menu);

        buttonsHandler();
    }

    /**
     * Handles button's actions
     */
    private void buttonsHandler() {
        compositeDisposable = new CompositeDisposable();

        final Button categoriesButton = findViewById(R.id.categoriesButton);
        categoriesButton.setOnClickListener(view -> startActivity(new Intent(this, GameCategories.class)));

        final Button playButton = findViewById(R.id.playButton);
        handleRandomCategoryPlayButton(playButton);

        final Button authorsButton = findViewById(R.id.authorsButton);
        authorsButton.setOnClickListener(view -> startActivity(new Intent(this, GameAuthors.class)));

        final Button statsButton = findViewById(R.id.statsButton);
        statsButton.setOnClickListener(view -> startActivity(new Intent(this, GameStats.class)));

        final FloatingActionButton fab = findViewById(R.id.FAB);
        fab.setOnClickListener(view -> startActivity(new Intent(this, GameHelp.class)));
    }

    /**
     * Handles action of play button click by starting game with random category
     *
     * @param playButton play button
     */
    private void handleRandomCategoryPlayButton(Button playButton) {
        playButton.setOnClickListener(view -> {
            final FirebaseAccessor firebaseAccessor = FirebaseAccessor.getInstance();
            final Disposable catNumSub = firebaseAccessor.getCategoriesNumber().subscribe(data -> {
                final Integer categoryId = ThreadLocalRandom.current().nextInt(1, data + 1);
                ArrayList<Integer> categoryIDs = new ArrayList<>();
                categoryIDs.add(categoryId);
                Intent intent = new Intent(this, GameMain.class);
                intent.putExtra("categoryIDs", categoryIDs); //Passes chosen categoryID to new opened activity
                startActivity(intent); //Starts GameMain Activity
            });
            compositeDisposable.add(catNumSub);
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}